/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

import {  
  NavigationActions,
  StackNavigator,
  TabNavigator 
} from 'react-navigation';

import Harry from './Harry'
import Elder from './Elder'
import Voldemort from './Voldemort'

const RootStack = StackNavigator({
  Harry: {
      screen: Harry
  },
  Elder: {
      screen: Elder
  },
  Voldemort: {
      screen: Voldemort
  }
})

const RootTab = TabNavigator({
  Harry: {
      screen: RootStack,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: (focused, tintColor) => (
            <Image style={{ width: 35, height: 35 }} 
                   source={require('../icons/HarryTab.png')} />
          ),
          tabBarOnPress: ({ scene, jumpToIndex }) => {
              navigation.dispatch(NavigationActions.reset({
                  index: 0,
                  key: null,
                  actions: [NavigationActions.navigate({ routeName: 'Harry' })]
              }))
            },
            title: 'Harry'
            
        }
      )
  },
  Elder: {
      screen: RootStack,
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: (focused, tintColor) => (
            <Image style={{ width: 35, height: 35 }} 
                   source={require('../icons/DumbeldoreBeard.png')} />
          ),
          tabBarOnPress: ({ prevScene, scene, jumpToIndex }) => {
              navigation.dispatch(NavigationActions.reset({
                  index: 0,
                  key: null,
                  actions: [NavigationActions.navigate({ routeName: 'Elder' })]
              }))
            },
            title: 'Dumbledore'
        }
      )
  },
  Voldemort: {
    screen: RootStack,
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: (focused, tintColor) => (
            <Image style={{ width: 35, height: 35 }} 
                   source={require('../icons/VoldemortIcon.jpg')} />
          ),
        tabBarOnPress: ({ prevScene, scene, jumpToIndex }) => {
            navigation.dispatch(NavigationActions.reset({
                index: 0,
                key: null,
                actions: [NavigationActions.navigate({ routeName: 'Voldemort' })]
            }))
          },
          title: 'Voldemort'
      }
    )
}
});

export default class App extends Component {
  render() {
    return (
      <RootTab/>
    );
  }
}
