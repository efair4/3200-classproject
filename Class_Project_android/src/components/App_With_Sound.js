/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Button,
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import Sound from 'react-native-sound';

export default class Sound extends Component {
  constructor(props) {
    super(props);
    this.state =
      {
        debug: true,
        loadedSounds: [],
      }
  }

  render() {
    return (
      <View style={styles.container}>
        <Button style={styles.testBtn} title="wand_1.mp3" onPress={() => this.playSound("wand_1.mp3")}></Button>
        <Button style={styles.testBtn} title="expelliarmus.mp3" onPress={() => this.playSound("expelliarmus.mp3", 1300)}></Button>
        <Button style={styles.testBtn} title="wingardium_leviosa.mp3" onPress={() => this.playSound("wingardium_leviosa.mp3")}></Button>
        <Button style={styles.testBtn} title="expecto_patronum.mp3" onPress={() => this.playSound("expecto_patronum.mp3")}></Button>
      </View>
    );
  }

  componentDidMount() {
    /* Load all sounds */
    /* Sounds are stored in android/app/src/main/res/raw
       Sounds must be all lowercase and use the file extension
       https://github.com/zmxv/react-native-sound
    */
    this._loadSound("wand_1.mp3");
    this._loadSound("expelliarmus.mp3");
    this._loadSound("wingardium_leviosa.mp3");
    this._loadSound("expecto_patronum.mp3");
    /* End of load sounds */
  }

  _getSoundIndex(soundName)
  {
    let soundIndex = -1;
    if (soundName) {
      for (var i = 0; i < this.state.loadedSounds.length; ++i) {
        if (this.state.loadedSounds[i].name && this.state.loadedSounds[i].name.toLowerCase() == soundName.toLowerCase()) {
          soundIndex = i;        }
      }
    }
    return soundIndex;
  }

  playSound(soundName, duration) {
    if (this.state.debug) {
      console.log("Starting play sound.");
    }
    let soundIndex = this._getSoundIndex(soundName);
    if (soundIndex != -1) {
      this.state.loadedSounds[soundIndex].callObj.play((success) => {
        if (success) {
          if (this.state.debug) {
            console.log('Finished playing sound.');
          }
        } else {
          console.error("Sound with name '" + soundName + "' could not be played media encoding error occurred.");
          this.state.loadedSounds[soundIndex].reset();
        }
      });
      if (duration && !isNaN(duration)) {
        setTimeout(() => { console.log(this.state); this.state.loadedSounds[soundIndex].callObj.stop() }, duration);
      }
    }
    else {
      console.error("Sound with name '" + soundName + "' could not be played because it has not been loaded into state.");
    }
  }

  _loadSound(soundName) {
    let sound = new Sound(soundName, Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.error("Failed to load sound with name: " + soundName, error);
      }
      if (this.state.debug) {
        console.log("Sound loaded: " + soundName);
      }
      this.setState()
      {
        let newSoundArray = [];
        let soundObj = {
          callObj: sound,
          name: soundName,
          volume: 1,
        };
        newSoundArray.push(soundObj);
        this.setState((prevState, props) => ({
          loadedSounds: prevState.loadedSounds.concat(newSoundArray)
        }));
      }
    });
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  testBtn: {
    fontSize: 30,
    paddingTop: 10,
  }
});
