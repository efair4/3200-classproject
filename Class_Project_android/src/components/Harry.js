import React, {Component} from 'react';
import {
    Image, 
    View
} from 'react-native'

import styles from '../styles/styles'
export default class Harry extends Component
{
    static navigationOptions = {
        title: 'Harry\'s Wand'
    }
    constructor(props)
    {
      super(props);
  
      this.state = {
        color: 'green'
  
      }
    }
    render()
    {
        let pic = require('../pictures/HarrysWand.png');
        return (
            <View style = {styles.wand}>
                <View style = {styles.spellArea}>
                <View style = {style = {height:80, width: 10, backgroundColor: this.state.color}}> </View>                </View>
                
                <View style = {styles.harryContainer}>
                <View style = {styles.harryWand}>
                <Image source={pic} style = {styles.wandImage}/>
                </View>
                </View>
            </View>
        );
    }
}
// import React, { Component } from 'react';
// import { 
//     Image,
//     View 
// } from 'react-native';
// import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures'
// export default class Harry extends Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//           gestureName: 'none',
//           backgroundColor: '#fff'
//         };
//       }

//     onSwipeRight(gestureState) {
//         this.setState({myText: 'You swiped right!'});
//       }
      
//     onSwipe(gestureName, gestureState) {
//         const {SWIPE_RIGHT} = swipeDirections;
//         this.setState({gestureName: gestureName});
//         switch (gestureName) {
//           case SWIPE_RIGHT:
//             this.setState({backgroundColor: 'yellow'});
//             break;
//         }
//       }

//     render() {
//         const config = {
//             velocityThreshold: 0.3,
//             directionalOffsetThreshold: 80
//           };
//         let pic = require('../pictures/HarrysWand.png');
//         return (
//         <GestureRecognizer
//         onSwipe={(direction, state) => this.onSwipe(direction, state)}>
//         config={config}
//         style={{
//           flex: 1,
//           backgroundColor: this.state.backgroundColor
//         }}
//             {/* <View>
//                 <Image source={pic} style = {{width: 300, height: 200}}/>
//             </View>  */}
//         </GestureRecognizer>
//         );
//     }
// }