import React, {Component} from 'react';
import {
    Image, 
    View
} from 'react-native'
import styles from '../styles/styles'
export default class Voldemort extends Component
{
    static navigationOptions = {
        title: 'Voldemort\'s Wand'
    }
    constructor(props)
    {
      super(props);
  
      this.state = {
        color: 'red'
  
      }
    }
    render()
    {
        let pic = require('../pictures/VoldemortWand.png');
        return (
            <View style = {styles.wand}>
            <View style = {styles.spellArea}>
            <View style = {style = {height:80, width: 10, backgroundColor: this.state.color}}> </View>            </View>
            <View style = {styles.voldWand}>
            <Image source={pic} style ={styles.voldWandImage}/>
            </View>
            </View>
        );
    }
}