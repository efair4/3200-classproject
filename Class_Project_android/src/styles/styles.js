import {StyleSheet} from 'react-native'
export default StyleSheet.create({
    wand:
    {
        flex: 1,
        justifyContent: 'center',
    },
    voldWand:
    {
        flex:6,
        justifyContent: 'center',
        alignItems: 'center',
    },
    harryContainer:
    {
        flex: 6
    },
    harryWand:
    {
        //flex:6,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 0.5
    },
    elderContainer:
    {
        flex: 6
    },
    elderWand:
    {
        //flex:6,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 1
    },
    wandImage:
    {
        height:470,
        width: 400,
    },
    voldWandImage:
    {
        height:510,
        width: 400
    },
    spellArea:
    {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
  });